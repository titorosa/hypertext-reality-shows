---
type: episode
title: Episode 6
---

Abruptly, I awaken, finding myself still confined within the [[ResortParaiso|resort]].

Today 6 crew members fainted.

Today total terror.

...

...

Participating in the Sunset Serenade Showcase, I poured my heart into composing a love song for my partner. Despite my efforts, we lost the challenge.

In a confessional, I reflect on the failed challenge and express my emotions towards Violeta, succumbing to despair as tears flow freely on camera.

The producer administers medication, a nameless pill with a prescribed dosage.

...

I drink 9 rum colas.

Tensions flare as I engage in a heated argument with Claire, the air thick with animosity.

Having received the fewest audience votes, Berta is eliminated from the competition. She picks up her suitcase and the limo drives her to the airport.

I am unable to sleep, my mind racing with thoughts of the future.

< [[PM-s2e5|Prev Episode]]

[[PM-s2e7|Next Episode]] >
