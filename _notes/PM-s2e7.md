---
type: episode
title: Episode 7
---

I wake up with a gasp.

...

...

...

...

Engaging in gossip with Andres and Tonio, we share whispered secrets in the shadows, seeking distraction from our shared predicament.

As the heat index soars, we are forced into lockdown, the extreme weather conditions rendering outdoor activities impossible.

Having received the fewest audience votes, Violeta is eliminated from the competition. She picks up her suitcase and the limo drives her to the airport.

...


Succumbing to exhaustion, I drift into sleep, seeking refuge from the chaos of reality.


< [[PM-s2e6|Prev Episode]]

[[PM-s2e8|Next Episode]] >
