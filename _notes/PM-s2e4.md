---
type: episode
title: Episode 4
---

I wake up sweating.

Overwhelmed by depression, I forgo my workout routine, opting instead to sit listlessly on the InterwovenLove Couch.

...

...

During a confessional with the producer, I am confronted with an invasive question: "What is your darkest secret, and how does it haunt you?" Answer: "My darkest secret is..."

The producer administers medication, a nameless pill with a prescribed dosage.

...

...

Having received the fewest audience votes, Ze is eliminated from the competition. They pick up their suitcase and the limo drives them to the airport.

I fall asleep dreaming of the end of the world.

< [[PM-s2e3|Prev Episode]]

[[PM-s2e5|Next Episode]] >
