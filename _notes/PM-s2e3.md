---
type: episode
title: Episode 3
---

The cold, automated lights flicker on promptly at 5 am.

...

...

I receive a text message from the audience: MAKE BREAKFAST FOR VIOLETA.

...

...

The sky looks like a wound.

I consume 15 beers.

Having received the fewest audience votes, Alia is eliminated from the competition. She picks up her suitcase and the limo drives her to the airport.

I fall asleep.

< [[PM-s2e2|Prev Episode]]

[[PM-s2e4|Next Episode]] >
