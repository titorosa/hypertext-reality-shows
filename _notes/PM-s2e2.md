---
title: Episode 2
type: episode
---

 I wake up with a gasp.

 ...

Despite the early hour, the oppressive heat weighs heavily on me, exacerbated by the absence of water in the dried-up pool.

...

...

I drink 10 pina coladas.

Having received the fewest audience votes, Chita is eliminated from the competition. She picks up her suitcase and the limo drives her to the airport.

Drifting into slumber, I am plagued by a haunting dream: my skin melting into the bed.


| [[PM-s2e3|Next Episode]] >
