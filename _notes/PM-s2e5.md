---
type: episode
title: Episode 5
---

...

As a heat wave blankets the surroundings, I am forced indoors, the oppressive temperature causing profuse sweating and headaches.

In the AC I proceed with my routine: 50 push-ups, 50 sit-ups, and 20 pull-ups.

Methodically grooming myself, I spent 20 minutes brushing my teeth, 10 minutes shaving, and an additional 10 minutes applying cologne and hair gel.

...

I ask the producer to watch the news but they say not right now.

The producer administers medication, a nameless pill with a prescribed dosage.


< [[PM-s2e4|Prev Episode]]

[[PM-s2e6|Next Episode]] >
