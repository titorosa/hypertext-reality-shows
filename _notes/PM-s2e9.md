---
type: episode
title: Episode 9
---

...

13 resort staff fainted.

Indulging in tropical drinks, I drank 4 piña coladas and 3 mai tais.

I vomit

...

Forming an alliance with Cecílio Julián at the Zen Zone, we shared whispered secrets and forged a bond.

I have a sudden premonition of the future or a feeling that I've already lived this sequence before.

...

Having received the fewest audience votes, Cecílio Julián is eliminated from the competition. He picks up his suitcase and the limo drives him to the airport.

During the Final Match, I am voted to be coupled with Tatiana, the outcome a testament to the bonds forged and the alliances formed throughout the competition.

We grab our suitcases and the limo drives us to the airport.

...

The sky looks like a wound.

In a disturbing dream, I keep opening doors and finding myself in the same room.


< [[PM-s2e8|Prev Episode]]
