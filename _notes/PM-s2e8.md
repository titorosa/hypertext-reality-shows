---
type: episode
title: Episode 8
---

I wake as the automated lights flicker on promptly at 5 am.

...

...

...

...

Having received the fewest audience votes, Tonio is eliminated from the competition. He picks up his suitcase and the limo drives him to the airport.

In a disturbing dream, I find myself engulfed in a raging fire. My body twists and contorts.

< [[PM-s2e7|Prev Episode]]

[[PM-s2e9|Next Episode]] >
