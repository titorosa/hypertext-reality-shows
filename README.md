[![Netlify Status](https://api.netlify.com/api/v1/badges/8cfa8785-8df8-4aad-ad35-8f1c790b8baf/deploy-status)](https://app.netlify.com/sites/digital-garden-jekyll-template/deploys)

# Hypertext: Reality Shows

A proposal for a hypertext fiction.

## License

Source code is available under the [MIT license](20%20Professional/[21]%20Portfolio/portfolio-site/LICENSE.md).
